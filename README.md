# README #

Repository of San Francisco Geographic Information Systems Program, server documentation

### Featuring detailed build, configure, and maintain instructionsr. ####

* Liberated from the confines of MS Word in 2017.02 and set free by Gitbook.io
* version of 2017-02-22
* Content author: Brian B. Quinn, Ph.D. GISP

### Featured Gitbooks ###

* sgs7 the Standard Geospatial Server based on CentOS 7

### Contact Information ###

Brian B. Quinn, Ph.D. GISP
Architect / Analyst, San Francisco GIS Program
Department of Technology, City and County of San Francisco
One South Van Ness Ave, 2nd Floor
San Francisco, CA 94103
+1.415.581.4083

### Additional Credits ###

Landing Page: Dimension by HTML5 UP
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
AJ
aj@lkn.io | @ajlkn


