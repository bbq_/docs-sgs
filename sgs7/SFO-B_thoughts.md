##SFO-B thoughts
The SFO-B projection has been developed to guide work at San Francisco International Airport (KSFO)

| Long_ctr_DD | Lat_ctr_DD | Azimut_deg |  scale |
| -- | -- | -- | -- |
| -122.3939412704 | 37.6289686531 | 27.7928209333 | 0.9999968 |