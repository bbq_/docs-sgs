##SGS7 version: System of Record users
| Username | Department | Password | status |
| -- | -- | -- |
| postgres | DT/SFGIS | _see Runbook_ | db superuser |
| sde | SFGIS | _see Runbook_ | Esri db superuser |
| sfar | ASR | arsf2011 | read-only user |
| sfcon | CON | consf2011 | read-only user |
| sfdcp | CPC | dcpsf2011 | read-only user |
| sfdcyf | CHF | dcyfsf2011 | read-only user |
| sfdem | ECD | demsf2011 | read-only user |
| sfdoe | ENV | doesf2011 | read-only user |
| sfdph | DPH | dphsf2011 | read-only user |
| sfdpw | DPWBSM | dpwsf2011 | read-only user |
| sfdpwboe | DPWBoE | dpwboesf2011 | read-only user |
| sfdt | TIS | dtsf2011 | read-only user |
| sffd | FIR | fdsf2011 | read-only user |
| sfgis | TIS | _see Runbook_ | db_owner |
| sfgsa | ADM | gsasf2011 | read-only user |
| sfhsa | HSA | hsasf2011 | read-only user |
| sfia | AIR | iasf2011 | read-only user |
| sfmo | MYR | mosf2011 | read-only user |
| sfmta | MTA | mtasf2011 | read-only user |
| sfpd | POL | pdsf2011 | read-only user |
| sfport | PRT | portsf2011 | read-only user |
| sfpuc | PUC | pucsf2011 | read-only user |
| sfrpd | REC | rpdsf2011| read-only user |
