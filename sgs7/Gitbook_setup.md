##Gitbook service setup on SGS7
####1. Standalone server: Gitbook :octocat: command line interface
_Suitable for single system or documentation server_

Based on information found, unsurprisingly, at [GitHub's GitbookIO command line interface](https://github.com/GitbookIO/gitbook-cli)

Prerequisite is to have NodeJS available on the server, along with `npm` Node Package Manager 
```
# yum install epel-release
# yum install nodejs
# yum install npm
# node --version
 v0.12.2        {viewed on 2017-02-08}
```

Then it's easy to install the Gitbook-cli globally for all users
```
# npm install -g gitbook-cli
```

The Gitbook installation appears to reside near `/usr/lib/node_modules/gitbook-cli`  As the path implies, the Gitbook is a NodeJS module named _gitbook-cli_

Basically, content for the book is a directory of Markdown files, and once the server has the Gitbook client installed, that directory can be served on a given pair of ports.  One port is pointed to by the reader's browser,and another port appears to be used to send messages to the Gitbook service.

Tuning the book involves adjusting objects in the `book.json` file found in the directory with all that book's Markdown content.

####2. Standalone server: Run Gitbook
Once you're ready to go, the [Gitbook Seup and Installation](https://github.com/GitbookIO/gitbook/blob/master/docs/setup.md) can help.

Customizing a bit based on information from [gitbook-plugin-advanced-emoji](https://www.npmjs.com/package/gitbook-plugin-advanced-emoji)

Once you get it working, :stuck_out_tongue_winking_eye:you'll likely want an [Emoji Cheat Sheet](http://www.webpagefx.com/tools/emoji-cheat-sheet/)
```
# npm install -g gitbook-plugin-advanced-emoji
# npm install -g gitbook-pdf
```
then edit _book.json_ to include
```
{
        "plugins": ["advanced-emoji"]
}

# gitbook install gitbook-plugin-advanced-emoji
# su - ags_install
$ gitbook install
$ gitbook --browser chrome --port 3000 serve &
```
The `chrome` browser flag seems to do the best job of waking up tabs in Chrome that are pointed at the documentation, and refreshing those pages on any save event in the content Markdown directory.

####3. Distributed documentation service: Documentation Server
In this case, we use the central storage machine `sfgis-inimg` and create a documentation export, publishing `/data/docs` for use by other SGS7 servers.

This requres an edit to `/etc/exports` on the documentation server.  The `rw` status is not essential, as Markdown edits take place in a single reference directory on the documentation server.   The Gitbook service, which does require read/write access, need only run on the documentation server, and through the export, all other SGS7 systems can mount the export and publish the `_book` directory of html content.

The line added to  `/etc/exports` on the documentation server was this
```
/data/docs 10.1.15.108(rw,sync) 10.250.60.11(rw,sync) 10.250.60.69(rw,sync) 10.250.60.80(rw,sync) 10.250.60.81(rw,sync) 10.250.60.82(rw,sync) 10.250.60.83(rw,sync) 10.250.60.84(rw,sync) 10.250.60.101(rw,sync) 10.250.60.103(rw,sync) 10.250.60.104(rw,sync) 10.250.60.180(rw,sync) 10.250.60.201(rw,sync) 208.121.200.218(rw,sync) 208.121.200.219(rw,sync) 208.121.200.227(rw,sync) 208.121.200.229(rw,sync)
```
Then, on every server that will be mirroring the documentation, it's possible to mount that export.

####4.  Documentation Mirror Server: create service account `gbook`
This account can reflect the ownership of the content that will be mounted.
For consistency of access acrosss the machines for the documentation share, consider foring the user ID to be consistent, for example `2001` on all the documentation machines will cause the mounted content to appear owned.
```
# adduser -g installer gbook
# passwd gbook
# usermod -u 2001 gbook
```
> :white_check_mark: Record this credential in the _Runbook_

It may be prudent to verify that the new `gbook` user is listed in `/etc/group` as a member of `installer` and if not, append it to the like in this style
```
installer:x:505:postgres,tom8,ags_install,gbook
```
####5.  Documentation Mirror Server: mount documentation content
That documentation mount can be done like this, where the documentation server has been aliased to `sg102`
```
# cd /data
# mkdir /data/docs
# chown gbook:installer /data/docs
# mount -t nfs4 sg102:/data/docs /data/docs
```
If `mount` doesn't work, then you need to run `# yum install nfs-utils` and try again.

If the name like `sg102` does not resolve, add it to `/etc/hosts`

And to make the mount persistent, a line can be added to `/etc/fstab` like this
```
sg102:/data/docs    /data/docs     nfs4    defaults        0 0
```
####6.  Documentation Mirror Server: add URL path to mounted documentation
On the mirror server's httpd configuration, simply set an alias to bring the `_book` directory to a convenient high level location like `/doc` with an edit to  `httpd.conf` and a restart of the `httpd` service.

Recall that 

```
Alias /doc /data/docs/sgs7/_book
<Directory "/data/docs/sgs7/_book">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>


</Directory>
```
To be prudent for public-facing servers, consider restricting access to this directory by modifying the `AllowOverride` line to instead be like:
```
    AllowOverride AuthConfig
```
then create a local password file in a consistent location.  In that way, the mirrored content can carry along its `.htaccess` file for all machines, and the individual servers may or may not enable access control to documentation---gererally restricted for public-facing machines and perhaps open for private network machines.
```
# htpasswd -c /etc/httpd/.htpasswd reader  (for both CentOS 6 / CentOS 7)
```
> :white_check_mark: Ensure that this credential is saved in the runbook for the servers that are enabling the restricted content

The access control file .htaccess must be kept at the root level of the shared content, which in this example is `/data/docs/.htaccess` and its content should have something like this
```
AuthType Basic
AuthName "SGS7 Documentation (Restricted)"
AuthUserFile /etc/httpd/.htpasswd
Require valid-user
```

then restart with one of these lines:
```
# systemctl restart httpd.service   ( CentOS 7, or )
# apachectl2 restart  ( SGeoS, or )
# service restart httpd  ( generic CentOS 6 )
```
After restart, the documentation server's live content directory can be viewed by simply trailing `/doc` after the server name---for any server that mounts the documentation share.  If the VM hasn't been running `httpd` before, then be sure to edit the `ServerName` and related alias fields in `httpd.conf` while you're there.

Once `httpd` is running, test the home page appearance to ensure that it presents the correct system name and links to its own mirrored documentation.
Home page content location varies by machine, but follow the `DocumentRoot` identifier in whatever `httpd.conf` is active.  Document Root may be found at one of the following locations, depending on how the SGS7 machine is configured.  The target of interest is probably `index.html`
```
/var/www/html
/htdocs
/htdocs/twbs
/htdocs/twbs/bower_components/bootstrap
```
If cloning many machines, one trick is to simply copy the last-used `index.html` in the following way
```
# cd /var/www/html
# mv index.html index_old.html
# wget http://10.250.60.104/index.html
# vi index.html
```
And then customize it for the machine.

####7. Bitbucket Git setup
Sometimes, it is just easier to clone the empty repository then copy initial directories of content into it.
```
# su - gbook
$ cd /data
$ git clone https://bbq_@bitbucket.org/bbq_/docs-sgs.git
$ git init
$ git add --all
$ git config --global user.email "brian.quinn@sfgov.org"
$ git config --global user.name "bbq_"
$ git commit -m "first"
$ git remote add origin https://bbq_@bitbucket.org/bbq_/docs-sgs.git
$ git push -u origin master
```