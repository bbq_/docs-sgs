## Standard Geospatial Server 7 (SGS7) Build Book

The San Francisco Enterprise Geographic Information Systems Program (SFGIS,) as a unit of the Digital Services Division of the Department of Technology, offers a variety of Enterprise GIS resources to the entities (Departments, Commissions, Agencies, Bureaus, etc.) of the City and County of San Francisco, California.

Among the more centralized areas of service for SFGIS is provision of geospatial servers, and this book documents how a Standard Geospatial Server (SGS) is presently built by SFGIS.  As of 2017-02, the base platform is CentOS 7.3 with PostgreSQL 9.6, and if needed Esri ArcGIS Enterprise 10.5 with various other features.  This CentOS-7-based configuration bears the moniker SGS7.

_This book (a Gitbook or Gbook) expands on a lengthy (~100-page) MS Word document with name like "SFGIS_Server_build_Notes_SGS7" or similar.  This Gbook should be available for reference when creating, configuring, or maintaining SGS7 servers._

An SGS7 server is intended to be configurable for a wide variety of uses--not all at the same time for a given machine.  As such, the stacks that have been built onto them are somewhat extensive.

![The SGS7 technology stack of 2017-01-30](img/SGS7_stack_v201609_20170130q.jpg)

This is how the SFGIS systems (mostly Standard Geospatial Servers) are deployed currently.  Some share similar configurations, but others are set to emphasize database, application serving, and may also include some narrow roles like documentation distribution.

![SFGIS geospatial servers, without EAS](img/systems_arch.jpg)
---

> This book was created by :octocat:Brian B. Quinn, Ph.D. GISP and distributed to support the SFGIS Coordinator Collective, representing interests of the rather numerous entities [Department, Commission, Agency, Bureau, etc.] of the City and County of San Francisco.
