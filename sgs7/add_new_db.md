##Create (PostGIS + ArcSDE) Databases as Required

####1. Use Esri tools for Create Enterprise Geodatabase
To create an Esri Enterprise Geodatabase, which loads the SDE schema, and installs the functions bundled within `libst_raster_pg.so`, `PGSQLEngine.so`, and `st_geometry.so`, one must have an authorization file.  When Esri ArcGIS Enterprise 10.5 Basic was installed, there is a keygen result buried at
```
/age105/arcgis/server/framework/runtime/.wine/drive_c/Program Files/ESRI/License10.5/sysgen/keycodes
```
So if that file is copied up to /htdocs/twbs or other handy place with SMB sharing, then it will be possible to reference this file when running the Esri tool **Create Enterprise Geodatabase** so copy that over and make it visible from a folder in Windows Explorer.  Its path can be entered in UNC form like:
```
\\dbdevo-smb-twbs\twbs\keycodes
```

In ArcGIS Pro, the relevant tool can be found at **Toolboxes > Data Management Tools > Geodatabase Administration > Create Enterprise Geodatabase** 

![Create Enterprise Geodatabase tool](/img/create_EntGDB.jpg)

Create Database(s) for Departments or Dev/QA/Prod Environments

####2. Add PostGIS functions to the Esri Enterprise Geodatabase
Into this tablespace, one per storage device please, the Esri tool can be used to create the necessary databases.  Within  `psql` we will add PostGIS functions, then using an Esri Desktop product like ArcGIS Pro, we can Create Enterprise Geodatabase to add the Esri functions that make it a happy SDE home.
```
# su - postgres
$ psql
postgres=# \c test
You are now connected to database “test” as user “postgres”.

CREATE EXTENSION postgis;

CREATE EXTENSION postgis_topology;

CREATE EXTENSION fuzzystrmatch;

CREATE EXTENSION postgis_tiger_geocoder;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE public.geometry_columns TO sde;

GRANT SELECT ON TABLE public.spatial_ref_sys TO sde;

```
That's the usual mix.

####3. Enable access to the new databases with Host-Based Authentication
The new database(s) that have been created must be allowed for access, which is specified in the PostgreSQL Host-Based Authentication file `pg_hba.conf`, edited as OS user `postgres`. The changes shown below create access for the `sde` user that owns Esri ArcSDE tables, the `dba` user that can be used for backup scripting, an `sfgis` user that is the prototype for department-level table ownership, and the `sfgis-user` that is the prototype for read-only table access.
```
# Initial accounts
# TYPE  DATABASE(S)     USER            ADDRESS                 METHOD
host    all             sde             127.0.0.1/32            md5
host    all             sde             10.0.0.0/8              md5
host    all             dba             127.0.0.1/32            md5
host    all             dba             10.0.0.0/8              md5
host    postgres        sfgis           127.0.0.1/32            md5
host    postgres        sfgis           10.0.0.0/8              md5
host    postgres        sfgis_user      127.0.0.1/32            md5
host    postgres        sfgis_user      10.0.0.0/8              md5
host    test            sfgis           127.0.0.1/32            md5
host    test            sfgis           10.0.0.0/8              md5
host    test            sfgis_user      127.0.0.1/32            md5
host    test            sfgis_user      10.0.0.0/8              md5
host    devo            sfgis           127.0.0.1/32            md5
host    devo            sfgis           10.0.0.0/8              md5
host    devo            sfgis_user      127.0.0.1/32            md5
host    devo            sfgis_user      10.0.0.0/8              md5
host    qual            sfgis           127.0.0.1/32            md5
```
If running, load the changes with
```
$ pg_ctl reload
```

####4. Adding Esri Enterprise Geodatabase functions to an existing database
If Create Enterprise Geodatabase is being applied to an _existing_ database that already has The PostgreSQL user then `sde` must be given database superuser privileges temporarily so that it can create new tables in the db system `postgres` database.  A handy way to do that is with pgAdmin 4.
Just navigate to the `sde` role and click the pencil icon.
![SDE privileges boost](/img/sde_priv_upgrade.jpg)
Then in the privileges tab of the dialog that opens, set Superuser to Yes and Save.
![sde SU](/img/sde_priv_save.jpg)

Creation of the geodatabase may have followed something along these lines:
```
# su - postgres
$ psql
postgres=# CREATE DATABASE test_0 OWNER sde ENCODING ‘UTF8’ TABLESPACE db_96;
CREATE DATABASE
postgres=# \c sfgis_dat0
You are now connected to database “sfgis_dat0” as user “postgres”.
postgres=# CREATE SCHEMA sde AUTHORIZATION sde;
CREATE SCHEMA
postgres=# GRANT ALL ON SCHEMA sde TO sde;
GRANT
postgres=# GRANT USAGE ON SCHEMA sde TO PUBLIC;
GRANT
```
After that, the **Create Enterprise Geodatabase** tool should run with warnings that the database exists already, that the `sde` user exists already, and such.  But it should work OK.
