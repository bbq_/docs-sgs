
# Summary

* [*SGS7 Documentation*](_SFGIS_doc_titles.md)
* [Introduction](README.md)
* [SFGIS's Motley Crew of Servers](VM_summaries.md)
* [Initialize a new VM image](init_new_VM.md)
* [Configure Windows SMB Share](config_SMB.md)
* [Gitbook server setup](Gitbook_setup.md)
* [Add or Modify Disks](add_drive.md)
* [PostgreSQL Installation](postgresql_install.md)
* [Add PostGIS+SDE Databases](add_new_db.md)
* [Administrativa](admin_notes.md)
* [System of Record users](SoR_users.md)
* [SFO-B thoughts](SFO-B_thoughts.md)
* [About the Author](about_the_author.md)