##Configure Samba SMB service
####_Windows file server protocol_ run on Linux

####1. Enable SMB for Windows workstation users
Enable SMB connections so Windows workstations can be connect to an SGS7 server, particularly those found deployed through City and County of San Francisco. 

SMB can be a less secure means of sharing storage because it is designed to be compatible with systems using old and insecure approaches to exposing storage space.  To make this a clean connection, we’ll configure both iptables to open only the minimum required connection types.

Because it is more secure than Workgroup shares, SGS7 configure SMB to only work with ports used by Active Directory systems.  Workgroup sharing uses ports that are kept closed.

It’s easy enough to install the system standard SMB server, but important to configure firewall, and essential to configure the actual SMB shares.
For Active Directory only we can add these to `/etc/sysconfig/iptables` (note two “udp” changes in the second line)
```
-A INPUT -s 10.0.0.0/8 -m state --state NEW -m tcp -p tcp --dport 445 -j ACCEPT
-A INPUT -s 10.0.0.0/8 -m state --state NEW -m udp -p udp --dport 445 -j ACCEPT
```
Then run `# systemctl reaload iptables.service` and continue by installing Samba
```
yum install samba samba-client samba-common
```
Verify the installed version; at CentOS 7.3 we get 4.4.4
```
smbd --version
```
If desired, label the served directory to let SELinux know it’s OK to share
```
semanage fcontext -a -t public_content_rw_t ‘/ags1041(/.*)?’
```

Set the Samba services to start at boot time.
```
systemctl enable smb.service
systemctl enable nmb.service
```
With an enterprise install, the standard configuration is found at `/etc/samba/smb.conf` and should have a section like the following to enable a share around ArcGIS Server.  In general, deeper shares with restricted users and allowed client IP are strongly preferred for better server security.
```
[global]
   netbios name = sfgis-portal
   netbios aliases = sfgis-portal.sfgov.org,208.121.200.229
   workgroup = AD
   server string = SGeoS Enterprise GIS Portal
   security = user
   passdb backend = tdbsam
   wins support = no
   interfaces = lo eth0 208.121.200.229
   hosts allow = 173.31.  208.70.

[age105]
   comment = ArcGIS Enterprise 10.5 install
   path = /age105
   public = no
   writable = yes
   printable = no
   browseable = yes
   create mask = 0644
   valid users = ags_install
```
It might be desirable to configure Samba to use Active Directory, and according to some [documentation](http://wiki.samba.org/index.php/Setup_a_Samba_AD_Member_Server) it is necessary to have precise time within an AD network, including both a running ntpd and ntp-signd daemons.
NTP steps are described in this book's section [Initialize a new VM Image](/init_new.md) were adapted from [Digital Ocean recommended steps](https://www.digitalocean.com/community/tutorials/additional-recommended-steps-for-new-centos-7-servers)

Then, set the SMB password for the ags_install user who owns the share.
Note that the accounts are created as system accounts, and file system permissions track with those system accounts, but Samba has its own separate password store for controlling SMB access.
This adds the new user’s password to the Samba password store.
```
smbpasswd –a ags_install
```
**NOTE:** when using an SMB share from a Windows workstation, connecting to DMZ servers we can’t rely on Active Directory for login, so be certain to clear the domain from the challenge screen.

The Samba login is based on a local login on that server, not AD. Backslash `\ ` before Samba user name, and either `\ags_user`  or `\\server-name\ags_user` should work correctly to log into the Samba share.
Clearing the domain with a leading `\ ` does work, like this sequence:

![Map Network Drive (not a letter drive!)](/img/map_net_drive.jpg)
![SMB login no AD, local user](img/smb_login.jpg)
 