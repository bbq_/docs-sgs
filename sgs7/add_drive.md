#Adding new storage drives to an SGS VM

###Maintenance Activity for SGeoS/SGS7 _Administrative procedure_

Sometimes a new disk is needed, or the last partition on a disk needs to be extended to add storage.
It’s possible to have the VM re-scan its SCSI bus to find new storage that has been provisioned by our virtualization team.

```
# yum install sg3_utils    << if necessary to have rescan-scsi-bus.sh >>
# cd /sys/class/scsi_host/host0
# rescan-scsi-bus.sh       << or  echo “- - -“ > ./scan  >>
# ls –l /dev/sd*              << expect to find something new like /dev/sdd >>
```

Worst case, it may be necessary to restart the machine to have it recognize the new scsi device.
Then edit partitions, and after that, format the partition, and after that, mount the new formatted parititon.

```
# fdisk –l /dev/sdd       << list device size >>
# parted /dev/sdd         << edit partition table on new device >>
(parted) print            << display current partition information >>
(parted) mklabel gpt      << doesn't write label, defines FS type >>
(parted) unit %
(parted) mkpart primary ext4 0 100%
(parted) print
(parted) align-check optimal 1
(parted) quit

# mkfs.ext4 /dev/sdd1
# cd /
# mkdir backup
# mount /dev/sdd1 /backup
# df –h
```
Then if you like the result, you can add a line at the end of `/etc/fstab`
like this:

```
/dev/sdb1       /data           xfs     defaults        0 0
/dev/sdc1       /pg_xlog        xfs     defaults        0 0
/dev/sdd1       /backup         xfs     defaults        0 0
```
