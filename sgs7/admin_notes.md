##Server Administrative Notes

####1. Restart SGS: Checklist 
Maintenance Activity for SGeoS/SGS7   **Administrative procecure**

#####Esri ArcGIS for Server machines
Sometimes circumstances require the restart of a multi-tenant machine.
The most complex situation is our public multi-tenant server  `sfgis-svc.sfgov.org` and the same procedure without 

Services to ensure are running:
1.  **PostgreSQL**
```
# su - postgres
$ gopg
$ pg_ctl start  (or pg_ctl stop –m fast; pg_ctl start)
$ exit
```
2.  **ArcGIS for Server** (implicit: Apache httpd, Tomcat for ArcGIS Web Adaptor)
```
# su - ags_install
$ cd /age105/arcgis/server (or /ags103/arcgis/server or such)
$ stopserver.sh
$ startserver.sh
$ exit
```
3.  **ArcGIS Web Adaptor**
```
su - ags_install
$ cd /usr/share/tomcat1/bin
$ ./startup.sh
```
4.  **Samba SMB service**
```
# service smb restart   (CentOS 6 machines)
# systemctl restart smb.service   (CentOS 7 machines)
```
5.  **Mapbox Tilestream Server** on `tile.sfgis-svc.sfgov.org`
```
# su - tom8
$ cd /htdocs/tiles
$ tilestream.sh
```
6.  **DPW Hydraulic web app** on `boe.sfgis-svc.sfgov.org`
Just verify that it is running

7.  **Adaptive landing page** on `sfgis-svc.sfgov.org`
Verify that it is running

8. **restart NFS service**  on `sfgis-inimg.sfgov.org`
```
# service nfs restart
```

9. **Gitbook server**  optional on `sfgis-inimg.sfgov.org`
```
# su - gbook
# cd sgs7
# gitbook --browser chrome --port 3000 serve &
```

#####Esri Portal for ArcGIS machines like `sfgis-prt1`

1. Portal for ArcGIS (implicit: Tomcat for ArcGIS Web Adaptor)
```
# su - ags_install
$ cd /usr/share/tomcat1/bin
$ ./startup.sh
$ cd /age105/arcgis/portal
$ stopportal.sh
$ startportal.sh
$ exit
```

####2. Verify current NodeJS and Upgrade
This information has been adapted from [work by Rahul K.](http://tecadmin.net/upgrade-nodejs-via-npm/#)
This is a sequence to display the version, and use a previously-installed `npm` to update Node itself so that, for example, Gitbook will run better.  To change globally, this was run su.

```
# node-v
# npm cache clean -f
# npm install -g n
# n stable
     install : node-v7.4.0
       mkdir : /usr/local/n/versions/node/7.4.0
       fetch : https://nodejs.org/dist/v7.4.0/node-v7.4.0-linux-x64.tar.gz
######################################################################## 100.0%
   installed : v6.9.1

# ln -sf /usr/local/n/versions/node/7.4.0/bin/node /bin/node
```
####3. Install NFS, configure for shares, and secure
Install nfs and prepare to configure for sharing among other SGeoS servers in our network. These instruction are adapated from [Srijan Kishore](https://www.howtoforge.com/nfs-server-and-client-on-centos-7)

Even the clients should get NFS installed
```
# yum install nfs-utils
```
Only on a server that is publishing an NFS share, edits must be made to `/etc/exports` and be sure to share to IP addresses to avoid _name lookup race_ exploits.
```
[root@sfgis-inimg bbq]# cat /etc/exports
/ags1022 10.250.60.0/24(rw,sync)
/opt/installs 10.250.60.0/24(rw,sync,no_root_squash,no_all_squash)
/gdata 10.1.15.108(rw,sync) 10.250.60.11(ro,sync) 10.250.60.69(ro,sync) 10.250.60.80(ro,sync) 10.250.60.81(ro,sync) 10.250.60.82(ro,sync) 10.250.60.83(ro,sync) 10.250.60.84(ro,sync) 10.250.60.101(ro,sync) 10.250.60.103(ro,sync) 10.250.60.104(ro,sync) 10.250.60.180(ro,sync) 10.250.60.201(ro,sync) 208.121.200.218(ro,sync) 208.121.200.219(ro,sync) 208.121.200.227(ro,sync) 208.121.200.229(ro,sync)
```
Edits must be made to `/etc/hosts.deny` to cover unallowed hosts
```
portmap:ALL
lockd:ALL
mountd:ALL
rquotad:ALL
statd:ALL
```
Edits must be made to `/etc/hosts.allow` for other SGeoS servers using the share
```
portmap: 10.250.60.101
lockd: 10.250.60.101
rquotad: 10.250.60.101
mountd: 10.250.60.101
statd: 10.250.60.101
```
Lock NFS services to specific ports in `/etc/sysconfig/nfs`  to configure the firewall
```
MOUNTD_NFS_V2="no"
MOUNTD_NFS_V3="yes"
RQUOTAD_PORT=875
LOCKD_TCPPORT=32803
LOCKD_UDPPORT=32769
MOUNTD_PORT=892
STATD_PORT=662
STATD_OUTGOING_PORT=2020
```
And ports must be opened in `/etc/sysconfig/iptables` to other SGeoS servers

 

Then restart the services to enable the shares to the speificed
```
service iptables restart
service rpcbind restart
service nfs restart
```
After this, from allowed host it should be possible to mount the share
```
sfgis-portal# mount –t nfs4 sg102:/gdata /gdata

sfgis-portal# df -h
/dev/sda3        65G   41G   21G  67% /
tmpfs           3.9G     0  3.9G   0% /dev/shm
/dev/sda1       477M  149M  303M  33% /boot
/dev/sdb1        20G  1.3G   18G   7% /data
/dev/sdc1        20G  172M   19G   1% /pg_xlog
/dev/sdd1        20G   44M   19G   1% /backup
sg102:/gdata    3.0T  2.7T  169G  95% /gdata
```
It's prudent to edit `/etc/fstab` to anticipate a desire to mount the share again following any system restarts, adding a line like
```
sg102:/gdata            /gdata                  nfs4    defaults        0 0
```

####4. ArcGIS Server Enterprise Basic: Enable REST Endpoints
Granted, with Server Enterprise Basic, you know that there are no Map services to be published.  All the same, as part of the ArcGIS Server installation, all the framework has been put in place to expose this.  One only needs turn it on.  This does **not** enable map services, but it definitely makes it more efficient to monitor service heartbeat by providing API access.  Enjoy.
Following information [Esri published here](http://resources.arcgis.com/en/help/rest/apiref/admin.html) point a browser to the Server's Administration page.

If the situation is particularly gnarly, on the server there are CLI tools in a path like `/age105/arcgis/server/tools/passwordreset` to manually reset the password.

On the admin page like `https://sfgis-db.sfgov.org:6443/arcgis/admin/` 
It is possible to navigate down to the page where Services Directory can be enabled like
```
https://sfgis-db.sfgov.org:6443/arcgis/admin/system/handlers/rest/servicesdirectory/edit
```
Which will make sure to bring up the REST endpoints on port :6080 by default.
One way to simplify this without the bother of Esri Web Adaptor is to configure Apache reverse proxy for this URI.  Edit:
```
# vi /usr/local/httpd/conf/httpd.conf    ( CentOS 6 SGeoS )
# vi /etc/httpd/conf/httpd.conf   ( CentOS 7  SGS7 )
```
Then append this sort of phrasing
```
ProxyRequests Off
ProxyPreserveHost On
ProxyPass /arcgis http://localhost:6080/arcgis
ProxyPassReverse /arcgis http://localhost:6080/arcgis

<Location /arcgis>
   Require all granted
</Location>
```

