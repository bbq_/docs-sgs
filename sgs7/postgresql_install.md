##PostgreSQL installation
####Forced by Esri to use _9.5_ in SGS7 of 2017-02

This document makes reference to the [Postgresql.org respository list](https://yum.postgresql.org/repopackages.php) and the [Yum Installation summary](https://wiki.postgresql.org/wiki/YUM_Installation).  It is noted that as of 2017-02-09 the most current Esri-supported major version for ArcGIS Enterprise 10.5 is only _PostgreSQL 9.5_ and 9.6 lacks available object libraries to implement Esri ST_GEOMETRY functions.

**PGDG** is the glorious people's PostgreSQL Global Development Group

Their binaries are exceedingly useful, reliable, and efficient ways to install PostgreSQL and to keep it updated.  This goes much more than double for PostGIS, where it's profoundly tedious to
[build it by hand,](superseded/build_postgis_21.md) so **don't do that!**

####1. Credits
The desire for near-effortless patching now outweighs the impulsive desire for latest version, so the following section has been informed by an approach in this [summary guide by Regina Obe and Leo Hsu](http://www.postgresonline.com/journal/archives/362-An-almost-idiots-guide-to-install-PostgreSQL-9.6,-PostGIS-2.2-and-pgRouting-2.1.0-with-Yum.html)

So first navigate to the [PG RPM repository](https://yum.postgresql.org/repopackages.php) and find _Red Hat Enterprise Linux 7 - x86\_64_
and copy the relevant link.  Then it becomes possible to run something like the following by pasting the link into your PuTTY session.
```
# yum install https://download.postgresql.org/pub/repos/yum/9.5/redhat/rhel-7-x86_64/pgdg-redhat95-9.5-3.noarch.rpm
# yum list postgresql*
```
Per existing EAS data server configurations, data area goes in `/data` logs in `/pg_xlog` and backup in `/backup`
System prep adapted from instructions in  PostgreSQL Wiki and this posting as well.

####2.  Import PostgreSQL Global Development Group (PGDG) RPM packages for server
Confirm for the sake of double-checking the architecture and OS with these lines
```
uname –a
Linux sfgis-db.sfgov.org 3.10.0-514.6.1.el7.x86_64 #1 SMP Wed Jan 18 13:06:36 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

cat /etc/centos-release
CentOS Linux release 7.3.1611 (Core)
```
take a moment to revel in the awesome umbra of the PGDG efforts—which replaces about 20 pages of this build document—by glancing at a list of how many dependencies are resolved in this package, including CGAL, SFCGAL, GDAL, PostGIS, and pgRouting.
```
yum list | grep pgdg95
```
####3.  Install PGDG server packages
This includes a devel package to keep options open for non-packaged extensions.
```
yum install postgresql95 postgresql95-server postgresql95-libs postgresql95-contrib postgresql95-devel
```

_If things just don’t work out right, and a better way forward is found that requires change at an earlier step in the PostgreSQL installation process, it’s OK.  Document what’s going to change next time, and then:
yum erase postgresql95*_

set the new system PG admin account:
Be certain to record this setting in the runbook.
```
passwd postgres
```
>  :white_check_mark: Verify that this new OS password is recorded in the runbook

####4.  Ensure path to installed PG resources is included
This was necessary for the postgres user, and in testing for root.
```
# su - postgres
$ pwd
  /var/lib/pgsql
```
There, edit `.bash_profile` to append these lines:
```
PATH=$PATH:$HOME/bin:/usr/pgsql-9.5/bin
export PATH
alias gopg='cd $PGDATA'
```
then source these edits to make them active
```
$ source .bash_profile
$ which pg_ctl
  /usr/pgsql-9.5/bin/pg_ctl
```

####5.  Configure major PG locations
Before initializing postgresql, configure the file system for desired location of data and logs.
Since SGeoS machines are at times database servers, choosing root-level locations for data and logs seems merited.

Create a configuration setting for the system at `/etc/sysconfig/pgsql/postgresql-9.5` containing:
```
PGPORT=5432
PGDATA=/data/9.5/data
PGLOG=/pg_xlog/9.5/pg_xlog/pgstartup.log
```
If these directories will be the locations, then they’d better exist, be owned by `postgres`, grouped with `postgres`, and one should attempt to label an appropriate SElinux context with semanage.
```
mkdir /data/9.5
mkdir /data/9.5/data
mkdir /pg_xlog/9.5
chown -R postgres:postgres /data /pg_xlog
su - postgres
```
(we will be using cp –r from an initialized state to populate pg_xlog) 

####6.  Initialze PostgreSQL (one time only)
Carefully verify that your data area is prepared and writeable by postgres user, then initialize.
If mistakes are made, consider a cd into /data, then $ rm -Rf 9.5 to try once again.
Still as user `postgres` run:
```
$ initdb -D /data/9.5/data
```

Continue to tune the data area.  These locations reflect the SFGIS EAS data server style.
In the interest of SElinux harmony, do the cp, and do not use mv.
```
$ cd /data/9.5/data
$ cp  postgresql.conf  postgresql.conf.orig
$ cp  pg_hba.conf  pg_hba.conf.orig
$ cp -R  pg_xlog  /pg_xlog/9.5
```
Verify the size of the copy of pg_xlog
```
$ du -s pg_xlog 
$ du -s /pg_xlog/9.5/pg_xlog
```
If they are the same size, remove the original pg_xlog, and replace with a symbolic link to the copy
```
$ rm -rf pg_xlog
$ ln -s /pg_xlog/9.5/pg_xlog pg_xlog
```
This should leave the PG data directory looking like this:
```
$ ll
total 140
drwx------. 5 postgres postgres  4096 Feb  8 13:49 base
drwx------. 2 postgres postgres  4096 Feb  8 13:49 global
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_clog
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_commit_ts
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_dynshmem
-rw-------. 1 postgres postgres  4468 Feb  8 13:49 pg_hba.conf
-rw-------. 1 postgres postgres  4468 Feb  8 13:50 pg_hba.conf.orig
-rw-------. 1 postgres postgres  1636 Feb  8 13:49 pg_ident.conf
drwx------. 4 postgres postgres  4096 Feb  8 13:49 pg_logical
drwx------. 4 postgres postgres  4096 Feb  8 13:49 pg_multixact
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_notify
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_replslot
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_serial
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_snapshots
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_stat
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_stat_tmp
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_subtrans
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_tblspc
drwx------. 2 postgres postgres  4096 Feb  8 13:49 pg_twophase
-rw-------. 1 postgres postgres     4 Feb  8 13:49 PG_VERSION
lrwxrwxrwx. 1 postgres postgres    20 Feb  8 13:54 pg_xlog -> /pg_xlog/9.5/pg_xlog
-rw-------. 1 postgres postgres    88 Feb  8 13:49 postgresql.auto.conf
-rw-------. 1 postgres postgres 22286 Feb  8 13:49 postgresql.conf
-rw-------. 1 postgres postgres 22286 Feb  8 13:50 postgresql.conf.orig
```

####7.  Start PostgreSQL and verify it runs; set superuser password
As the postgres user, start the service.  If from root use `# su - postgres` to load the `postgres` environment variables.
Of particular importance are `PGDATA=/data/9.5/data` and `PGLOG=/pg_xlog/9.5/pg_xlog`
```
# su - postgres
$ pg_ctl start
```
That should work, so next use psql create a test user and schema to validate connections.
But first set your postgres db user (db super user) password
```
$ psql
postgres=# ALTER USER postgres WITH PASSWORD ‘<newpassword>’;
ALTER ROLE
postgres=# \q
```
>  :white_check_mark: Now is the time to record this assignment in the run book

####7.  Open server (firewall) port to PostgreSQL service
Exit to root, edit `/etc/sysconfig/iptables` to open postgresql port :5432 with a line similar to this:
```
-A INPUT -s 10.0.0.0/8 -p tcp -m tcp --dport 5432 -j ACCEPT
```
Then restart iptables to read the new configuration
```
# service iptables restart
```
####8.  Configure PostgreSQL service to accept connections
/As user `postgres` edit some postgresql configuration files in `/data/9.5/data/`
```
# su - postgres
$ gopg    (aliased 'cd $PGDATA')
```
Edit `postgresql.conf` so that 
```
listen_addresses = 'localhost,10.250.60.104'
port = 5432
```
are uncommented and set properly for testing purposes.  For production this can later be locked down to 127.0.0.1/32 .

Edit `pg_hba.conf` so that it’s simplified to something like this, where the db users are configured to connect locally through loopback (127.0.0.1), which can work through an ssh connection, and for testing also the addresses of Windows workstations from which a GUI administration tool could be run (or simply private address range 10.0.0.0/8)
Using md5 requires that the postgres db user password has been set.
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                md5
host    all             postgres        127.0.0.1/32            md5
host    all             postgres        10.0.0.0/8              md5

# replication privilege.
#local   replication     postgres                                trust
#host    replication     postgres        127.0.0.1/32            trust
#host    replication     postgres        ::1/128                 trust

# user connections
# TYPE  DATABASE(S)     USER            ADDRESS                 METHOD
host    all             readonly        0.0.0.0/0               md5
host    all             editor          208.121.200.219/32      md5
host    all             sde             127.0.0.1/32            md5
host    all             sde             10.0.0.0/8              md5
host    all             dba             127.0.0.1/32            md5
host    all             dba             10.0.0.0/8              md5

# Initial accounts
#
host    postgres        sfgis           127.0.0.1/32            md5
host    postgres        sfgis           10.0.0.0/8              md5
host    postgres        sfgis_user      127.0.0.1/32            md5
host    postgres        sfgis_user      10.0.0.0/8              md5
host    devo            sfgis           127.0.0.1/32            md5
host    devo            sfgis           10.0.0.0/8              md5
host    devo            sfgis_user      127.0.0.1/32            md5
host    devo            sfgis_user      10.0.0.0/8              md5
host    qual            sfgis           127.0.0.1/32            md5
host    qual            sfgis           10.0.0.0/8              md5
host    qual            sfgis_user      127.0.0.1/32            md5
host    qual            sfgis_user      10.0.0.0/8              md5
host    prod            sfgis           127.0.0.1/32            md5
host    prod            sfgis           10.0.0.0/8              md5
host    prod            sfgis_user      127.0.0.1/32            md5
host    prod            sfgis_user      10.0.0.0/8              md5

```
restart postgresql to get these changes applied
```
# pg_ctl restart
``` 

####9.  Verify Connections function from Windows workstation
On the workstation, it’s possible to use a Windows GUI like _pgAdmin 4_ to confirm the configuration is working for remote access.  This example describes pgAdmin.  It might be necessary to ensure that server $HOSTNAME can be resolved by that name from the pgAdminIV client machine.  One way to do this in Windows is to add an entry in file C:\windows\system32\drivers\etc\hosts linking the hostname and its IP address, so that the name can be resolved locally.

Launch pgAdmin, and use File > Add Server… to open the New Server Registration dialog.
Input a reference name for the server in Name, the server’s IP address in Host, and consider testing connection to Maintenance DB postgres with user postgres if you’ve configured things as described above.

 

This should add a line to the Servers object in Server Groups of pgAdmin’s Object Browser.
 

Double-clicking the server object should expand it to show components of the PG instance.
 
####10. Add PostGIS functionality
Really not that hard thanks to the PGDG folks!

Choose from the selections of RPM at The [PG 9.5 repository](https://yum.postgresql.org/9.5/redhat/rhel-7-x86_64/) and grab a current link to follow this approach:
```
# cd /opt/installs
# wget https://yum.postgresql.org/9.5/redhat/rhel-7-x86_64/postgis2_95-2.3.2-1.rhel7.x86_64.rpm
# yum install postgis2_95-2.3.2-1.rhel7.x86_64.rpm
Complete!
```
That's all there is---and so much geospatial goodness follows!

####11. Add Esri ArcSDE functionality
If you've already got Esri ArcGIS for Server Basic installed, then you just copy the shared object libraries over for PostgreSQL to use to provide the ST_GEOMETRY and ST_RASTER functions.
```
# cd /usr/pgsql-9.5/lib
# cp /age105/arcgis/server/DatabaseSupport/PostgreSQL/9.5/Linux64/*.so .
```

####12. Secure db TCP/IP  Connections  with SSL
Once the connections have been verified as working, consider making a self-signed cert just for PostgreSQL to use.  Guidance from [cryptomonkeys](https://www.cryptomonkeys.com/2016/08/building-a-mailserver-part2/)

Stop the PostgreSQL service 
```
# su - postgres
$ pg_ctl stop -m fast
$ exit
```

Then generate a self-signed key and cert for use only by PostgreSQL
```
# cd /etc/pki/tls/private
# openssl req -newkey rsa:4096 -rand /dev/urandom -nodes -sha256 -out sfgis-db-pg_sha256.csr -keyout sfgis-db-pg_key4k.key
# openssl x509 -req -days 3650 -in sfgis-db-pg_sha256.csr -signkey sfgis-db-pg_key4k.key -out ../certs/sfgis-db-pg_sha256.crt
# openssl req -new -x509 -sha256 -extensions v3_ca -keyout sscakey.pem -out sscacert.pem -days 3650
```
Create directories and copy these PG-specific key and cert for postgres use
```
# mkdir /data/9.5/data/private
# mkdir /data/9.5/data/certs
# cp sfgis-db-pg_key4k.key /data/9.5/data/private
# cp ../certs/sfgis-db-pg_sha256.crt /data/9.5/data/certs
# chown -R postgres:postgres /data/9.5/data
# su - postgres
$ gopg
```
save a copy of `postgresql.conf` and proceed to edit the section near Security and Authentication, enabling enccryption
```
listen_addresses = 'localhost,10.250.60.104'
port = 5432
max_connections = 100
ssl = on
ssl_ciphers = 'HIGH:MEDIUM:+3DES:!aNULL'
ssl_cert_file = 'certs/sfgis-db-pg_sha256.crt'
ssl_key_file = 'private/sfgis-db-pg_key4k.key'
ssl_prefer_server_ciphers = on
ssl_ecdh_curve = 'prime256v1'
password_encryption = on
```
Save edits, then restart PostgreSQL
```
# su - postgres
$ pg_ctl restart
```
Verify the connection by disconnecting and reconnecting with pgAdmin 4.

####13. Create One Tablespace for the disk
This is a job for the `postgres` user.  Please note that as of PostgreSQL 9.5 and onward, it is no longer permissible to create tablespaces within your $PGDATA configuration directory.  Instead, place them on the /data device at its top level.
```
# su - postgres
$ cd /data
$ mkdir dat_95
$ chmod 700 !$
$ gopg
$ psql
```
Provide password for your PostgreSQL super-user `postgres`, not the OS user `postgres`, and head into PG.  First make sure that there is not already an 'sde' user, then create that role so that it can own the SDE databases.
```
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {} 

postgres=# CREATE ROLE sde LOGIN PASSWORD '<<pwd>>' NOINHERIT CREATEDB;
CREATE ROLE
postgres=# CREATE SCHEMA sde AUTHORIZATION sde;
CREATE SCHEMA
postgres=# GRANT USAGE ON SCHEMA sde TO PUBLIC;
GRANT
```
Now it's time to create the tablespace within the directory just created
```
postgres=# CREATE TABLESPACE dat_95 OWNER sde LOCATION '/data/dat_95';
CREATE TABLESPACE
```
