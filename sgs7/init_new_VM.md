#Base CentOS 7.0 system - SGS7
##Build steps for CentOS installation 

####1.  Identify target VM, validate provisioning
Start with blank system provisioned on SFGIS VM Host, currently 200P-vCProd02.ad.sfgov.org at 200 PAUL ST data center.  In the _DT collection of machines is the GIS group, where as of 2014.04.28 there are 46 Virtual Machine systems.  The SFGIS systems share subnet 10.250.60.x  and the VM naming convention starts each machine’s name with “SFGIS” and a three-digit expression of its IP address on the subnet.

New SFGIS server deployments are documented on a Citypedia page for the relevant subnet.
http://citypedia.sfgov.org/citypedia/index.php/Subnetwork_10.250.60_IP_Address_Allocation 

A Gitbook update of that page should be prepared.

A standard SGeoS image should have four virtual disks, and the last partition on any given disk should be arranged so that if any of them fill up the device can be extended.

Base resources are typically one or two vCPU of 2.67 GHz, 4GB of system memory per vCPU, and a total of 110 GB of storage with these initial allocations, thin provisioned to only consume space as used.
```
SCSI (0:0) 50 GB   for boot, swap, and system /
SCSI (0:1) 20 GB   for data (PostgreSQL PGDATA) as /data
SCSI (0:2) 20 GB   for PostgreSQL log files as /pg_xlog
SCSI (0:3) 20 GB   for database backup as /backup
```

####2.  Download new CentOS ISO
The CentOS ISO image was downloaded from a mirror found through the CentOS site at http://www.centos.org/download/ and saved to a local external (USB 3.0) drive with available space. Try the latest x86_64-DVD .iso

####3.  Flip the VMware vSphere BIOS @ Boot Switch
A ServiceNow ticket was opened to request enablement of a one-time Boot-to-BIOS for the VM, which makes it possible for those with mere VMware Operator privileges to boot from a local ISO image and then modify or replace the entire storage partitioning of the VM.
This ticket’s work was performed by Anthony Truong or Henry Chin of CCSF/DT/Virtualization team.  Note that the boot to BIOS is a setting that only works one time, so be certain to have the ISO image ready before attempting.

####4.  Install Server from CentOS Media
With the ISO mounted, a CentOS _minimal server_ configuration can be selected, but do not let the installer create partitions.

Three manually created partitions should be made on the first storage device.
Be certain not to use the default LVM (Linux Virtual Machine) partition type, and instead use standard unix file system devices, choosing type “ext4”

1. WELCOME TO CENTOS LINUX 7
 * English / English (United States)
 * Continue

2.  INSTALLATION SUMMARY:  Network & Host Name
 * enter Host Name or FQDN; Apply
 * \> Configure
 * Device ens32, can use drop down to identify MAC address
 * \> IPv4 Settings
 * Method: Manual
 * \> Add
```
Address   10.250.60.11
Netmask 255.255.255.0
Gateway 10.250.60.253
DNS1  10.1.3.236, 10.1.3.235
[x] Require IPv4 addressing for this connection to complete
```
 * Save
 * \> On
 * Done

3. DATE & TIME
 * Americas / Los Angeles
 * Network Time: On
 * Done

4. INSTALLATION DESTINATION
 * need three partitions on _sda_
 * select VMware Virtual disk 'sda'
 * Partitioning:  [x] I will configure partitioning
 * Done
 * New CentOS Linux 7 Installation
 * paritioning scheme: \> Standard Partition
 * \> CentOS Linux Linux 7.3.1611 for x86_64
```
/boot  600 MiB  'sda1'   # File System: 'ext4'  Label: 'boot'
swap   4 GB     'sda2'   # (~1x system memory)
/      ''       'sda3'   # 'xfs' (blank uses remainder of device space)
```
 * MANUAL PARTITIONING: Done
 * (review SUMMARY OF CHANGES)
 * Accept Changes

5. **Begin Installation**
6. Set a ROOT PASSWORD
>  :white_check_mark:  record it in an appropriate runbook

7. USER CREATION
 * make your personal user an administrator and set password memorably
 * Finish Configuration

>###¡Be Certain to Dismount Your Installation ISO!

(avoid placing a failed boot point for the soon-to-be-missing installation DVD as first choice for GRUB)
* **Reboot**

####5. Verify the installation and initialize iptables as firewall
2. Log in as yourself and run `$ sudo su`
3. Verify Internet connection with `# ping bq.bz` or similar
4. `# yum update`
5. `cd ~bbq`  or to your user's home directory to configure the firewall tables by copying _firewalld_ and _ip6tables_ rules into a starter for _iptables_ to use.  The following steps are taken from [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-migrate-from-firewalld-to-iptables-on-centos-7)
6. `# iptables -S | tee ./firewalld_iptables_rules`
7. `# ip6tables -S | tee ./firewalld_ip6tables_rules`
8. harmonize these two with their respective `/etc/sysconfig/iptables` and `/etc/sysconfig/ip6tables` without redundant lines, and this should save the default firewalld rules.
9. Verification can be done with
```
# sh -c 'iptables-restore -t < /etc/sysconfig/iptables'
# sh -c 'ip6tables-restore -t < /etc/sysconfig/ip6tables'
```

10. Install iptables with `# yum install iptables-services`
11. Stop _firewalld_ and start _iptables_
```
# systemctl stop firewalld
# systemctl start iptables
# systemctl start ip6tables
```
12. Verify that _firewalld_ is no longer running
```
# firewall-cmd --state
```
should show `not running`
13. Disable _firewalld_ and enable _iptables_ on system restarts
```
# systemctl disable firewalld
# systemctl mask firewalld
# systemctl enable iptables
# systemctl enable ip6tables
```

####6. Place SELinux in permissive mode

Based on [How to Disable SELinux](http://www.tecmint.com/disable-selinux-temporarily-permanently-in-centos-rhel-fedora/) by Aaron Kili at Tecmint.com

1. Edit `/etc/sysconfig/selinux` to set within it `SELINUX=permissive`
2. **Reboot**
3. Verify status with `sestatus` which should respond _disabled_
4. enjoy not having SELinux kill your attempts to set up ssh on `:2239`



####7. Disable ipv6 (for now)

Based on [How to disable IPv6 in CentOS 7](https://www.unixmen.com/disable-ipv6-centos-7/) by SK on unixmen.com

1. Edit `/etc/syscntl.conf` to add the following lines
```
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
```
2. make the change just entered happen with `sysctl -p`

####8. Enable ssh on a high port

1. `# vi /etc/ssh/sshd_config` for similar pattern (uncommented)
```
Port 2239
AddressFamily inet
ListenAddress 10.250.60.81
Protocol 2
PermitRootLogin no
```
2. `# systemctl start sshd.service`
3. Test a remote `ssh` login from PuTTY or similar client

####9. Tune SELinux (so as not to ignore it completely)

Then, update the Security-Enhanced Linux (SELinux) mechanism (and let sshd receive connections on this obscure port) by letting the kernel know what’s up.  Consider adding a policy utility like this
```
# yum -y install policycoreutils-python
```

This could take about 30 seconds or more, so be a little bit patient.  With that done, try this to add ssh-type actions on port 2239 to SELinux’s list of acceptable uses:
```
# semanage port -a -t ssh_port_t -p tcp 2239
```

Be certain to configure ssh clients to connect on this port (e.g. ssh -p 2239 for this_host)

####10. Install VMware Tools

Based on instructions from [VMWare tools CentOS 7 yum](http://partnerweb.vmware.com/GOSIG/CentOS_7.html)

`# yum install open-vm-tools`

####11. Prepare for secure connections: key, CSR, cert, cert chain and keystore
Put that shiny new openssl to use by generating a key for the system.  Here’s a way to create one using 4K bits and a pseudo-random device.
```
# cd /etc/pki/tls/private
# openssl req -new -newkey rsa:4096 -rand /dev/urandom -nodes -sha256 \
   -out sfgis-NAME.sfgov.org.sha256.csr -keyout sfgis-NAME_key4k.key
```
####12.  Set up network time daemon
Install ntp and set the service to launch on startup.
```
# yum install ntp ntpdate ntp-doc
```
Make an initial setting to the clock from the US Bureau of Standards WWV server, named after the radio frequency station WWV that broadcasts time on standard frequencies.  Then start the service.
```
# ntpdate wwv.nist.gov
# systemctl start ntpd.service
# systemctl enable ntpd.sercice
```
Review configuration of ntpd time sources if desired by editing `/etc/ntp.conf`

####13.  Install NFS, configure for shares, and secure (optional)
Install nfs and prepare to configure for sharing among other SGS7 servers in our network. These instruction are adapated from [NFS server and client on CentOS 7](https://www.howtoforge.com/nfs-server-and-client-on-centos-7) by Srijan Kishore 
1. Prerequisite are to have the packages installed 
```
# yum install nfs-utils
```
2. Edits must be made to /etc/exports share to IP to avoid name lookup race exploits
```
/ags1022 10.250.60.101(rw,sync,no_root_squash,no_all_squash)
/opt/installs 10.250.60.101(r,sync,no_root_squash,no_all_squash)
```
3. Edits must be made to `/etc/hosts.deny` to cover unallowed hosts
```
portmap:ALL
lockd:ALL
mountd:ALL
rquotad:ALL
statd:ALL
```
4. Edits must be made to `/etc/hosts.allow` for other SGS7 servers using the share
```
portmap: 10.250.60.101
lockd: 10.250.60.101
rquotad: 10.250.60.101
mountd: 10.250.60.101
statd: 10.250.60.101
```
5. Lock NFS services to specific ports in `/etc/sysconfig/nfs` to configure the firewall
```
MOUNTD_NFS_V2="no"
MOUNTD_NFS_V3="no"
MOUNTD_NFS_V4="yes"
RQUOTAD_PORT=875
LOCKD_TCPPORT=32803
LOCKD_UDPPORT=32769
MOUNTD_PORT=892
STATD_PORT=662
STATD_OUTGOING_PORT=2020
```
6. These ports must be opened in `/etc/sysconfig/iptables` to other SGS7 servers

7. Then restart the services to enable the shares to the speificed
```
service iptables restart
service rpcbind restart
service nfs restart
```
8. After this, from allowed host it should be possible to mount the share
```
# mount –t nfs4 sg102:/gdata /gdata
# df -h
 ```
 ####14. Mount drives for database applications
 Using information from [8 Linux 'parted' Commands](http://www.tecmint.com/parted-command-to-create-resize-rescue-linux-disk-partitions/) by Marin Todorov at Tecmint.com

 The SGS7 machines have four storage drives, and thus far only the first one with its three partitions have been mounted so that `cat /etc/fstab` should reveal
```sh
# /etc/fstab
# Created by anaconda on Tue Feb  7 08:48:05 2017
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
UUID=8a493cfb-975d-44eb-a5db-f0e79912e700 /     ext4    defaults  1 1
UUID=f11b3ae8-20b9-4ff5-b7e0-fc3d4859196e /boot ext4    defaults  1 2
UUID=e8a01c55-1bb9-4ba2-aff3-c22297c37395 swap  swap    defaults  0 0
```
This should be reflecting three block devices mounted thusly

|Block Device | Mount Point |
|-------------|-------------|
|/dev/sda1|/boot|  
|/dev/sda2|swap|
|/dev/sda3|/|
but we have three more devices to format and mount to complete this SGS7 starting template image.  All three of these default to 20 GB raw size and they should exist as raw devices
```
/dev/sdb  /dev/sdc  /dev/sdd
```
that we can partition and format for use by PostgreSQL or other purposes.

```
# parted
(parted) select /dev/sda
(parted) print
Model: VMware Virtual disk (scsi)
Disk /dev/sda: 53.7GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system     Flags
 1      1049kB  601MB   600MB   primary  ext4            boot
 2      601MB   3601MB  3000MB  primary  linux-swap(v1)
 3      3601MB  53.7GB  50.1GB  primary  xfs
```

Use _parted_ to format, or re-format as needed, the three additional drives that are there to configure PostgreSQL data, PostgreSQL logs, and local storate for backup.

```
# mkdir /data /pg_xlog /backup

# parted /dev/sdb
(parted) print    [just to see what's there, but we'll format anyhow]
(parted) mklabel msdos
(parted) unit %
(parted) mkpart primary xfs 0 100%
(parted) print
(parted) align-check optimal 1
(parted) quit
# mkfs.xfs /dev/sdb1
# mount /dev/sdb1 /data

# parted /dev/sdc
(parted) mklabel msdos
(parted) unit %
(parted) mkpart primary xfs 0 100%
(parted) align-check optimal 1
(parted) quit
# mkfs.xfs /dev/sdc1
# mount /dev/sdc1 /pg_xlog

# parted /dev/sdd
(parted) mklabel msdos
(parted) unit %
(parted) mkpart primary xfs 0 100%
(parted) align-check optimal 1
(parted) quit
# mkfs.xfs /dev/sdd1
# mount /dev/sdd1 /backup

# df -h        [human-readable format option]
```

#### The SGS7 system is now ready for more customization