##SFGIS Server Descriptions
During the SFGIS Coordinator Collective conference call of 2017-02-16, it was recognized that the server infrastructure was not as familiar among the City's GIS Coordinators as it might be.  As a demonstration of the efficiency with which the SFGIS distributed documentation system works, the following notes are being prepared to provide a sort of _personality sketch_ for each of the Standard Geospatial Servers (SGS) in our stable.

####1. Background: Virtual Machines as Cloud infrastructure
Foremost, all of the following descriptions refer to virtual machines, or VM, rather than distinct physical servers.  In popular terms, VM are a basic part of Cloud-whatever-it-might-be.  Each VM is defined by software running on a VM hosting cluster.  Specifically these server clusters are collections of identical blades using Intel microprocessors and running [VMware ESX for Intel or ESXi](http://searchvmware.techtarget.com/tip/VMware-vSphere-basics-ESXi-host-cluster-vCenter-and-shared-storage) (sometimes voiced "e-sexy" or just ESX) hosting software.  The ESXi servers are managed using VMware vSphere client or web interfaces that permit creation, modification, and destruction of individual VM.  These VM generally don't share memory or storage space, although they can draw from a pool of prcessor cores. In this way, the most limited resources on the ESXi hosts are system memory.  Storage is managed on separate systems [built from arrays of solid state drive](https://www.purestorage.com/products.html) as a Storage Area Network SAN, with space not shared once allocated to a specific VM.

In our system maps, individual VM have distinct icons, and some of these also have golden "tin can" icons attached to them to represent on-board databases.

####2. Background: Network Subnets and Security Regimes
The concatenation of all the world's visible computer networks forms the Internet.  Like a street, it's useful yet somewhat dangerous.  On it, packets of information may travel around the world to places good or bad, transiting between [Internet Protocol (IP) addresses.](http://ntrg.cs.tcd.ie/undergrad/4ba2/ipng/gerd.ipv4.html)

To improve security and permit re-use of some IP addresses, a small set of the possible IP version 4 (4-byte, or IPv4) addresses [have been set aside for the special purpose of **not** traveling the Internet.](https://www.arin.net/knowledge/address_filters.html)  These private addresses can be used to identify machines on a private, or local area network, but those specific addresses can't be reached across the Internet.  In that way, if a private network machine were to reveal its address as, say, 10.250.60.11 than no machine outside of that private network could use that address to directly reach or attack the machine.  For smaller home networks, addresses like 192.168.0.0/16, meaning the lowest 16 bits (or 64K or 65536) of addresses are available, which can be really helpful for those households having fewer than 65,000 networked devices. :grin:

Mediating the network space between a private network and the open Internet can be a demilitarized zone or DMZ subnet.  In this area, a few hardened or special-purpose machines are assigned routable addresses and so may be reached directly from the Internet.  However a typical DMZ has carefully managed firewalls to limit access from the Internet to the machine only for specific purposes such as an http [daemon](http://www.linfo.org/daemon.html) or web server.  Separately, another firewall is configured especially to provide asymmetrical access between the private network and the DMZ.  Typically this firewall allows connections from private network to DMZ more permissively, and connections established from DMZ, which might contain a compromised server, to private network are much more restricted.

####3. The SFGIS Standard Geospatial Server (SGS) system map
This table summarizes the machines described in this article

| *.sfgov.org | IP Address | Category | Summary Purpose |
| -- | -- | -- | -- |
| SFGISDB | 10.250.60.185 | Early Production | Enterprise System of Record |
| SFGISESRILM | 10.250.60.99 | Early Production | SFGIS Esri License Manger |
| datasf-pggeo | 10.250.60.103 | Geodata Servers | Data SF PostgreSQL |
| sfgis-fsrdb | 10.250.60.201 | Geodata Servers | Facilities System of Record |
| sfgis-db* | 10.250.60.104 | Geodata Servers | Multi-role GIS System of Record |
| sfgis-inimg | 10.250.60.102 | Multi-tenant | Internal Image Services |
| sfgis-insvc | 10.250.60.101 | Multi-tenant | Internal Multi-tenant Services |
| sfgis-img | 208.121.200.218 | Multi-tenant | External Image Services (in DMZ) |
|sfgis-svc | 208.121.200.219 | Multi-tenant | External Multi-tenant Services (DMZ) |
| sfmta-evt1 | 10.250.60.81 | ArcGIS Enterprise | SFMTA Geo Event server |
| sfgis-prt1 | 10.250.60.82 | ArcGIS Enterprise | SFGIS Community Portal for ArcGIS |
| sfgis-hsvc1 | 10.250.60.83 | ArcGIS Enterprise | Hosted GIS Services 1 of 2 |
| sfgis-hsvc2 | 10.250.60.84 | ArcGIS Enterprise | Hosted GIS Services 2 of 2 |
| sfgis-host1 | 10.250.60.11 | ArcGIS Enterprise | Portal-federated Hosting Server |
| sfgis-dsstbig | 10.250.60.80 | ArcGIS Enterprise | Spatio-temporal Big Data Store |
| sfgis-dstile | 10.250.60.69 | ArcGIS Enterprise | Vector Tile Data Store |
| sfgis-dsrel | 10.250.60.180 | ArcGIS Enterprise | Esri Relational Data Store |

This is the current diagram of the SGS system map:

![SGS systems current architcture](img/systems_arch.jpg)

The following descriptions will be sorted by the group heading shown at the top, or Internet area.

####4. Early Production: License Server & System of Record db
These are Windows Server 2008 R2 machines with specialized Esri purposes.

#####SFGISDB  Enterprise System of Record Geodatabase (Esri ArcSDE)
This system was established in 2012 based on PostgreSQL 8.3 and Esri ArcGIS 10.0 for Server Basic (ArcSDE.)  As of this writing, the system has been available for five years and is accessed by most all city agencies as the reference source for widely-distributed GIS features such as street centerlines and parcels.  Quite a few FME extract, transform, and load (ETL) scripts run nightly to keep key features up to date.

#####SFGISESRILM  The SFGIS Esri concurrent License Manager
This system was established in 2011 at the time of Esri ArcGIS 10.0 for Desktop.  The license server stays current with the latest Esri release and as of this writing is at 10.5.0 version.  The server runs a very small daemon to listen for Esri license requests and check out concurrent licenses to the requestors.  This approach allows the city to re-use Esri licenses and to centralize the Esri provisioning as shown here:
![Esri Licenses snapshot](img/Esri_LM_20170221.jpg)

####5. Geodata Servers
These are Standard Geospatial Servers configured as relational database machines.

#####datasf-pggeo DataSF PostgreSQL Geodatabase resource
Configured for relational database. Provides an environment to support DataSF ETL work.  As of 2017-02, it runs CentOS 7.2 on 2 vCPU with 8 GB memory.

#####sfgis-fsrdb SFGIS Facilities System of Record Database (multi-role)
Configured as ArcGIS 10.3 for Server Basic Enterprise with multiple role databases.  Hosts the reference lists of city facilities, as used in multiple departments. As of 2017-02, three PostgreSQL 9.2 databases provide development (Devo,) quality assurance (Qual,), and production (Prod) tiers used for ETL and its development under Esri ArcGIS 10.3, and the system runs CentOS 6.6 on 1 vCPU with 4 GB memory.

#####sfgis-db*  SFGIS System of Record (multi-tier)
Configured for multi-role relational database.  Hosting for an updated enterprise GIS system of record.  As of 2017-02, four PostgreSQL 9.5 databases provide testing (Test,) development (Devo,) assurance (Qual,) and production (Prod) tiers for distributing geospatial data under ArcGIS 10.5 Enterprise. The machine has multiple DNS entries and can be found under the tier-oriented names of sfgis-dbdevo, sfgis-dbqual, sfgis-dbprod but also sfgis-db for administrative purposes.  At 2017-02 the server runs CentOS 7.3 on 2 vCPU with 8 GB memory.

####6. ArcGIS 10.3 for Server Multi-tenant Deployment
The following machines are either a part of, or related to the base ArcGIS Enterprise deployment, and all feature some aspect of Esri ArcGIS 10.5 Enterprise.

#####sfgis-inimg  Internal Image Server (Multi-projection)
Configured as Esri ArcGIS 10.3 Server Standard Enterprise with Image Server extension. Esri Image extension publishes Esri Mosaic Datasets as Esri Image services, so one disk image can be published in multiple projections  This machine has large storage device to locally host orthoimagery or grid files.  That storage is shared to the other SGS machines over an [NFS share.](https://en.wikipedia.org/wiki/Network_File_System)  That way, this machine does serve imagery, but the same imagery is served from the DMZ by sfgis-img with the storage mounted over NFS.  As of 2017-02 the server runs CentOS 6.7 on 2 vCPU with 10 GB memory and a 3 TB disk that is typically mounted at `/gdata`

#####sfgis-insvc  Internal Esri Server (Multi-tenant)
Configured as ArcGIS 10.3 Server Standard Enterprise with no extensions, this machine provides most administrative access to nine tenancies.  Mounts the large share from `sfgis-inimg` as `/gdata` to provide a local path to orthoimagery for use in published map services. Tenants have their own SDE database over PostgreSQL 9.4 and PostGIS 2.1, published services folder, and web content directory that is exposed from the server.  Tenants are able to publish their own Esri services and support their private network applications from this machine.  As of 2017-02 the server runs CentOS 6.8 with 2 vCPU and 8 GB memory.

#####sfgis-img  External Image Server (Multi-projection)
Configured as ArcGIS 10.3 Server Standard Enterprise with Image Server extension.  Mounts the large NFS share as `/gdata` so that all image services follow a local path and can access the reference copy of uncompressed full-resolution imagery.  Esri Image Server extension allows publication of Esri Mosaic Datasets, so that a single image on disk can be published in multiple projections as Esri Image Services.  This is not a multi-tenant machine.  The system hardware was augmented to the demands of starting many image services across several projections.  As of 2017-02 the server runs CentOS 6.8 with 4 vCPU and 16 GB memory.

####sfgis-svc External Esri Server (Multi-tenant)
Configured as ArcGIS 10.3 Server Standard Enterprise with no extensions, this machine hosts 16 tenancies.  It mounts the large `/gdata` share so that tenants have a local path to full-resolution orthos.  There is a local PostgreSQL 9.4 instance loaded with PostGIS 2.1 functions. The system hardware has been augmented as the number of tenancies has grown.  As of 2017-02 the server runs CentOS 6.8 with 2 vCPU and 10 GB memory.

####7. Multi-tier Base ArcGIS Enterprise Deployment
The following machines are either a part of, or related to the base ArcGIS Enterprise deployment, and all feature some aspect of Esri ArcGIS 10.5 Enterprise.