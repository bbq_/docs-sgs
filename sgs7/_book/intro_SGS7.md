# Standard Geospatial Server 7 (SGS7)

The San Francisco Enterprise Geographic Information Systems Program (SFGIS) offers a variety of Enterprise GIS resources to the entities (Departments, Commissions, Agencies, Bureaus, etc.) of the City and County of San Francisco, California.

