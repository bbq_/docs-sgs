## Standard Geospatial Server 7 - Superseded Build Notes

All things must pass, and finely crafted documentation is no exception.

This directory contains Markdown that is no longer current for the SGS7 build.
In some cases, these were early attempts to build latest-version componewnts from source, and over time that approach has been abandoned in favor of a more enterprise-centric one that uses `yum` and RPM repositories that are much more easily kept current.

> This book was created by :octocat:Brian B. Quinn, Ph.D. GISP and distributed to support the SFGIS Coordinator Collective, representing interests of the rather numerous entities [Department, Commission, Agency, Bureau, etc.] of the City and County of San Francisco.
