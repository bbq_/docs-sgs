##Build PostGIS 2.1.3 with SFCGAL geometry and GTK+ annotation

PostGIS is a very powerful and highly interdependent Open Source GIS tool.
Despite its [light and attractive project page](http://postgis.net/) and logo...
![cute](../img/PostGIS_logo.png)

numerous PostGIS dependencies, some of which have themselves numerous dependencies,  can turn a source build process into a wrestle with a snarling beast.![]](../img/LoTR_war_mammut.jpg)

The following install procedure began by adapting a [particularly insightful and succinct blog post](http://dragkh.wordpress.com/2012/05/18/compile-and-install-postgis-2-0-0-on-replicated-postgresql-9-1-3-at-amazon-web-services-ec2-s3) and continued to ensure that all available PostGIS 2.1 extensions, and its most precise geometric capabilities were enabled.

**The wiser choice by far** is to use the PGDG package rather than building this all from source. http://yum.postgresql.org/9.5/redhat/rhel-6-x86_64/ 


Open Source is a capable and powerful approach, yet each contributor’s work becomes most efficient when only filling gaps between existing tools.  As a result, a high-level project such as PostGIS is built with many dependencies on the components it has connected; these dependencies may themselves have several levels of dependencies beneath that provide the capabilities used in that project.
Every one of these gets and clones is used in subsequent parts of this most-involved step, where the extended functions of PostGIS 2.1 have been enabled.  Most steps are built from source to incorporate the latest stable build.  As with all unix command lines, each and every character should be viewed as important. Places in this document where that’s the case  have been shown in Courier font.

 
####1.  Start from a CentOS system 6 PostgreSQL 9.2 _[written 2014-05]_

####2.  Option A: Install PostGIS from package.  This is the clever choice.
```
# wget http://yum.postgresql.org/9.5/redhat/rhel-6-x86_64/postgis2_92-2.1.3-1.rhel6.x86_64.rpm
# yum install postgis2_92-2.1.3-1.rhel6.x86_64.rpm
```

####3.  Option B (steps 4--31) build PostGIS from source with latest stable versions
Ensure that you have a couple of days to get through this with testing.

####4.  Obtain PostGIS and extensions sources
As used in subsequent steps for the first SGeoS testbed server.  This approach will help clarify the many components upon which PostGIS depends. Consider visiting the site in the leading parts of each URL then making an informed choice about which versions to download and build from source when starting fresh.
```
mkdir /opt/installs
cd !$
wget http://download.osgeo.org/postgis/source/postgis-2.1.3.tar.gz
wget http://download.osgeo.org/proj/proj-4.8.0.tar.gz
wget http://download.osgeo.org/geos/geos-3.4.2.tar.bz2
git clone https://github.com/json-c/json-c.git
wget https://s3.amazonaws.com/json-c_releases/releases/json-c-0.12-nodoc.tar.gz
wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.13.tar.gz
wget http://download.osgeo.org/gdal/1.11.0/gdal-1.11.0.tar.gz
wget http://sourceforge.net/projects/cunit/files/CUnit/2.1-2/CUnit-2.1-2-src.tar.bz2
wget http://sourceforge.net/projects/freetype/files/ftjam/2.5.2/ftjam-2.5.2.tar.bz2
wget http://sourceforge.net/projects/dblatex/files/latest/download?source=files
wget http://www.cmake.org/files/v2.8/cmake-2.8.12.2.tar.gz
wget http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2
wget https://ftp.gnu.org/gnu/gmp/gmp-6.0.0a.tar.bz2
wget http://www.mpfr.org/mpfr-current/mpfr-3.1.2.tar.bz2
wget http://download.qt-project.org/official_releases/qt/4.8/4.8.6/qt-everywhere-opensource-src-4.8.6.tar.gz
wget http://download.qt-project.org/official_releases/qt/5.3/5.3.1/single/qt-everywhere-opensource-src-5.3.1.tar.xz
wget http://www.algorithmic-solutions.info/free/Packages/LEDA-6.3-free-fedora-core-8-64-g++-4.1.2-mt.tar.gz
wget https://gforge.inria.fr/frs/download.php/file/33524/CGAL-4.4.tar.bz2
wget https://github.com/Oslandia/SFCGAL/archive/v1.0.4.tar.gz -O sfcgal-1.0.4.tar.gz
wget http://www.us.apache.org/dist/ant/binaries/apache-ant-1.9.4-bin.tar.bz2
wget http://pkgconfig.freedesktop.org/releases/pkg-config-0.28.tar.gz
wget ftp://sourceware.org/pub/libffi/libffi-3.1.tar.gz
wget ftp://ftp.gnome.org/pub/gnome/sources/glib/2.41/glib-2.41.2.tar.xz
wget ftp://ftp.gnome.org/pub/gnome/sources/gtk+/3.13/gtk+-3.13.5.tar.xz
wget ftp://ftp.stack.nl/pub/users/dimitri/doxygen-1.8.7.src.tar.gz
```
####5.  Configure a few dependencies
Using these prepared packages saves system build time
```
yum -y install tetex-tex4ht
yum -y install libxml2-devel
yum -y install ImageMagick*
yum -y install gcc-c++  json-c-devel.x86_64
yum -y install java-1.7.0-openjdk  java-1.7.0-openjdk-devel
yum -y install java-devel xerces-j2
yum -y install mesa-libGL mesa-libGL-devel mesa-libGLU-devel
```

_Notes regarding dependencies for building PostGIS with all extensions and documentation:_

- The teTeX package implemented TeX document typesetting for unix-like systems, the tex4ht package is the TeX for Hypertext, converting typeset technical documents into HTML and XML.
- The development packages for XML is a C library for eXtensibe Markup Language (XML) that is part of the GNOME (“genome”) project that builds desktop frameworks for Linux.  The libxml2 package is also used outside of the desktop environment to handle XML interchange in C programming.
- The ImageMagick package provides image file format conversion and image processing functions that can be used within several programming environments
- The GNU C++ compiler is a vast package to build, and the default version works for most source, and the JavaScript Object Notation development bundle is sought for by PostGIS
- Open JDK is an Open Source implementation of Java Standard Edition from Sun Microsystems prior to its acquisition by Oracle.  Open JDK is the reference implementation of the Java Development Kit.
- Java development tools are augmented by the Xerces2 Java Parser to implement XML schema
- mesa-libGL are OpenGL development packages installed from the MIT mesa implementation.
The main Mesa package was installed for ArcGIS, so expect only the devel packages to be added.
- Doxygen is a documentation generator used to build PostGIS documentation

####6.  Install proj-4 from source
The -j2 flag allows compilation in two threads for a 1-core machine. The PROJ.4 package performs all manner of geographic projections and transformations and is used by PostGIS.
```
cd /opt/installs
tar xvf proj-4.8.0.tar.gz
cd proj-4.8.0
./configure
make -j3
make check
sudo make install
sudo ldconfig
ln -s /usr/local/lib/pkgconfig/proj.pc /usr/lib64/pkgconfig/
```
####7.  Install GEOS from source
This requires the `c++` compiler if it’s not already installed. This is the Geometry Engine--Open Source (GEOS), ported from Java Topology Suite to C++, and is the PostGIS default for operations not performed by CGAL.
```
cd /opt/installs
tar xvf geos-3.4.2.tar.bz2
cd geos-3.4.2
./configure
make -j3
sudo make install
sudo ldconfig
```
####8.  Install json-c from source
On CentOS 6.5 this required an updated autogen as of 2014.05.19. The JavaScript Object Notation enables attribute-value pair object communication. JSON-C provides this capability for C language programming purposes and is used by PostGIS.
```
cd /opt/installs/
tar xvf json-c-0.12-nodoc.tar.gz
cd json-c-0.12
./autogen.sh
./configure
autoreconf -fvi
make -j3
sudo make install
sudo ldconfig
ln -s /usr/local/lib/pkgconfig/json-c.pc /usr/lib64/pkgconfig
```
####9.  Install HDF5 from source
Used by GDAL, takes over eight minutes to make a default config; the make check provides some peace of mind given the vast number of warnings thrown.  This is the Hierarchical Data Format developed by NCSA in the US, and used by Python, Matlab, and Java.
```
cd /opt/installs/
tar xvf hdf5-1.8.13.tar.gz
cd hdf5-1.8.13
./configure
make –j3
make check
sudo make install
```
####10. Install GDAL from source
Use latest Python 2.7.6 by running in virtualenv for this; be patient. This is the Geospatial Data Abstraction Library (GDAL) to transform vector and raster data formats through a common GDAL abstract raster type and a common OGR abstract vector data type.  With these OGR types merged, GDAL functions like an Open Source version of Safe Software FME.

The ERDAS ECW compression available in GDAL requires the SDK from ERDAS -> Hexagon -> Intergraph to work
```
cd /opt/installs
virtualenv venv
source venv/bin/activate
tar xvf gdal-1.11.0.tar.gz
cd gdal-1.11.0
./configure --with-python
make –j3
sudo make installl
ldconfig
deactivate
```
####11. Install CUnit from source
Supports unit testing; version 2.1-2 installs with configure, so use that version.  CUnit provides a testing framework that can be used by C programmers.  This module is used in PostGIS to construct standard test suites to verify function of code after it has been built.
```
cd /opt/installs
tar xvf CUnit-2.1-2-src.tar.bz2
cd CUnit-2.1-2
./configure
make
make install
ldconfig
ln -s /usr/local/lib/pkgconfig/cunit.pc /usr/lib64/pkgconfig
```
####12. Install dblatex from egg
Depends on earlier `easy_install` Python package when building new Python from source (Module-0, Step 8)  and a manual install of DocBook DTD and an initial catalog for PostGIS documentation.  The first computer-based typesetting system TeX was extended with macro tags to become LaTeX, and a specialized set of macros for technical documentation were applied to create DocBook LaTeX, or dblatex.  With it, one writes documentation once in a neutral format, then exports it to many different presentations.
```
cd /usr/local/share
mkdir xml  xml/docbook  xml/docbook/dtd  xml/docbook/dtd/5.0
cd !$
wget -O docbook5.dtd http://docbook.org/xml/5.0/dtd/docbook.dtd
cd /usr/local/share/xml
xmlcatalog --noout --create docbcatalog
xmlcatalog -noout --add ‘public’ ‘-//OASIS//DTD DocBook XML V5.0//EN’ \
  ‘file://usr/local/share/xml/docbook/dtd/5.0/docbook5.dtd’  docbcatalog
http://vault.centos.org/6.5/os/Source/SPackages/docbook5-style-xsl-1.75.2-4.el6.src.rpm
```
Install DocBook style sheets
```
yum install docbook5-style*
cd /usr/share/sgml/docbook
ln -s xsl-ns-stylesheets-1.75.2 xsl-stylesheets
easy_install dblatex
```
####13. Install CMake from source
Yet another build tool, this one used by CGAL.  The CGAL package is vast and written in C++, so a more powerful make tool was used than the C-oriented system defaults.
```
cd /opt/installs
tar xvf cmake-2.8.12.2.tar.gz
cd cmake-2.8.12.2
./bootstrap
make
make install
```
###14. Install Boost from source
A vast collection of C++ extensions used by CGAL; be patient as it can take 12 minutes to compile.  Boost libraries provide standard tools that underlie much of the numerical programming in CGAL, boosting the productivity of the programmers it serves.
```
cd /opt/installs
tar xvf boost_1_55_0.tar.bz2
cd boost_1_55_0
./bootstrap.sh
./b2
./b2 install --prefix=/usr/local
ldconfig
```
####15. Install GMP from source
This is the arithmetic library used by MPFR.  The recursive name GNU’s Not Unix (GNU) brands the original Open Source porting of Unix.  The GNU Multiple Precision  (GMP) arithmetic library overcomes common floating-point limits with extensible precision for calculations.
```
cd /opt/installs
tar xvf gmp-6.0.0a.tar.bz2
cd gmp-6.0.0
make
make check
make install
ldconfig
```
####16. Install MPFR from source
Multi-precision floating point library used by CGAL.  This is the GNU Multiple Precision Floating-point Reliably (MPFR) library, where you can divide by zero and not crash.
```
cd /opt/installs
tar xvf mpfr-3.1.2.tar.bz2
cd mpfr-3.1.2
./configure
make
make check
make install
```
####17. Install Qt4 from source
C++ programming framework used by CGAL; a **huge build** that could take 45 minutes or more to compile. The Qt (“cutie”) framework was developed through a company Quasar Technologies and provides a library of interface graphic widgets for designers across many platforms.  When Nokia bought Quasar, the name became Qt.
NOTE: ArcGIS mobile applications now use ArcGIS Runtime SDK for Qt, so this package can have use for both Esri and CGAL purposes.
```
cd /opt/installs
tar xvf qt-everywhere-opensource-src-4.8.6.tar.gz
mv  qt-everywhere-opensource-src-4.8.6  qt-4.8.6
export QTDIR=/opt/installs/qt-4.8.6
cd qt-4.8.6
./configure
o
yes
gmake
gmake install
```
####18. Install LEDA object libraries
Graph and network system used by CGAL.  The Library of Efficient Data Types and Algorithms (LEDA) is from Max Planck Institute provides computational geometry and graph theory algorithms.  Its distrubutor Algorithmic Solutions Software GmbH licenses source code for commercial use, and offers the library of binary functions for free.  We use the binaries.
```
cd /opt/installs
tar xvf LEDA-6.3-free-fedora-core-8-64-g++-4.1.2-mt.tar
mv  LEDA-6.3-free-fedora-core-8.64-g++-4.1.2-mt   LEDA-6.3
export LEDAROOT=/opt/installs/LEDA-6.3
```
####19.  Install CGAL from source
General-purpose spatial math processing from INRIA France wrappered by SFCGAL. The Computational Geometry Algorithms Library (CGAL) is a C++ library for efficient and reliable geometric algorithms.  It is available for use with Open Source software for free and is licensed for commercial use.  Because it incorporates arbitrary precision and floating-point reliability, it might produce more accurate and reliable spatial queries.  For this, it was considered worth the trouble of its many dependencies to build it into PostGIS.
```
cd /opt/installs
tar xvf CGAL-4.4.tar.bz2
cd CGAL_4.4
cmake .
(Note: in the 4.4 source, it was necessary to patch CGAL-4.4/src/CGAL_Qt4/all_files.cpp line to comment out line 2 with  // #include /opt/installs/CGAL-4.4/src/CGAL_Qt4/DemosMainWindow.cpp   to force not compiling the demos)
sed -i '2 s/^/\/\//' src/CGAL_Qt4/all_files.cpp
make
make install
```
####20. Install gcc 4.7.2 from developer resource
The SFCGAL team uses this newer-build C language compiler that provides syntax flexibility with ‘typename’ and they rely on it.  The leading part of environment variable PATH can be trimmed after compilation if desired to return to gcc 4.4.7 that comes with the CentOS 6.5 distribution
```
cd /etc/yum.repos.d
wget http://people.centos.org/tru/devtools-1.1/devtools-1.1.repo
yum --enablerepo=testing-1.1-devtools-6 install devtoolset-1.1-gcc devtoolset-1.1-gcc-c++
export CC=/
export PATH=/opt/centos/devtoolset-1.1/root/usr/bin${PATH:+:${PATH}}
```
####21. Install SFCGAL from source
The PostGIS wrapper for OGC objects in CGAL.  Simple Features in CGAL (SFCGAL) is an implementation of Open GIS Consortium (OGC) spatial object manipulations that can be accessed through an extended SQL syntax.
```
cd /opt/installs
tar xvf sfcgal-1.0.4.tar.gz
cd SFCGAL-1.0.4
cmake .
make
```
####22. Install Apache Ant from binaries
Depends on Open JDK and java-devel installed at Step 2.  The Ant installer is a build tool created in Java, and it is used by PostGIS to build some drivers that are written in Java.
Since it’s also needed for building Tomcat later, share it with a symbolic link in `/bin`.
```
cd /opt/installs
tar xvf apache-ant-1.9.4-bin.tar.bz2
cp –r apache-ant-1.9.4 /usr/share
export ANT_HOME=/usr/share/apache-ant-1.9.4
ln -s /usr/share/apache-ant-1.9.4/bin/ant /bin/ant
```
####23. Install pkg-config from source
`pkg-config` is used at libffi build time to query the system’s installed libraries.
```
cd /opt/installs
tar xvf pkgconfig-0.18.tar.gz
cd pkgconfig-0.18
./configure
make
make install
```
####24. Install libffi from source
Required for glib-2.0, this is the Foreign Function Interface library for C-language programming.  It allows code to dynamically call compiled functions by pointer rather than compiling the functions into the each module that uses it; elsewhere used in Python and Ruby.
```
cd /opt/installs
tar xvf libffi-3.1.tar.gz
cd libffi-3.1
./configure
make
make install
```
####25. Install glib-2.0 from source
Required for GTK+ graphics, the GLib bundle is five C-language system libraries developed by GNOME project that provides generic memory management and threading and the GLib object system.
```
cd /opt/installs
xz -d glib-2.41.2.tar.xz
tar xvf glib-2.41.2.tar
cd glib-2.41.2
./autogen.sh
./configure --enable-man=no
make
make install
```
####26. Install GTK+ from source
Depends on Open JDK and java-devel installed earlier, and on the GLib object system.  The GNU Image Manipulation Program (GIMP) is a raster data editor.  The enhanced GIMP Tool Kit (GTK+) is a set of interface graphic widgets.
```
cd /opt/installs
xz -d gtk+-3.12.2.tar.xz
tar xvf gtk+-3.12.2.tar
cd glib-2
```
####27. Install Graph visualization toolkit
Used by Doxygen for the PostGIS documentation generators.
```
wget http://www.graphviz.org/pub/graphviz/stable/redhat/el6/x86_64/os/graphviz-2.38.0-1.el6.x86_64.rpm
```
####28. Install Doxygen
Used by the PostGIS documentation generators.
```
cd /opt/installs
tar xvf doxygen-1.8.7.src.tar.gz
cd doxygen-1.8.7
./configure
make
make install
ldconfig
```
####29. Install PostGIS from source
Finally this is the actual PostGIS build. Note that as of 2014.05.28 it was necessary to apply a patch according to this OSGeo Trac ticket so that PostGIS can build against the latest json-c 0.12, where the error calls being used were deprecated.
```
su - postgres
cd /opt/installs
tar xvf postgis-2.1.3.tar.gz
cd postgis-2.1.3
./autogen.sh
./configure --with-gnu-ld
```
Hopefully yields something like the following:
![PostGIS success](../img/PostGIS_success.png)

Then apply the patch per http://trac.osgeo.org/postgis/ticket/2723
```
make -j2
make check
make install
```
No fireworks to celebrate, just a **shared object for PostgreSQL to use!**

####30. Now it’s time to enable PostGIS
In a first spatial test database, this example calls it `sp_uno` and we’ll also verify that PostgreSQL is already running by trying to start it.
```
pg_ctl start
createdb sp_uno
createlang plpgsql sp_uno
psql sp_uno

postgres=# CREATE EXTENSION postgis;
postgres=# CREATE EXTENSION postgis_topology;
postgres=# CREATE EXTENSION fuzzystrmatch;
postgres=# CREATE EXTENSION postgis_tiger_geocoder;
postgres=# \q
```
For full ability to work with rasters, set these environment variables in the system’s global `/etc/environment` file if they are always wanted.
```
POSTGIS_ENABLE_OUTDB_RASTERS=1
POSTGIS_GDAL_ENABLED_DRIVERS=ENABLE_ALL
```
When creating a new ArcGIS enterprise geodatabase, these scripts should be run to enable PostGIS in the database (each separate database needs these initializations and they do not appear to conflict with ArcGIS use of the same database).

_Example:_ for a freshly created ArcGIS database egdb1, as system user postgres run
```
cd /opt/installs/postgis-2.1.3
psql -d egdb1 -f  doc/postgis_comments.sql
psql -d egdb1 -f  spatial_ref_sys.sql
psql -d egdb1 -f  postgis/sfcgal.sql
```
that should leave 1068 functions in the PostgreSQL  public schema for db egdb1.

####31. Perform the PostGIS Garden Test to verify the install
In the documentation directory `/opt/installs/postgis-2.1.3/doc` there are source files that can be used to generate PostGIS documentation, as well as some testing tools.  Based on instructions posted here, it’s possible to configure global tests.  If this is the first time running these tests, fear no error when dropping a nonexistent testpostgis db.  These commands will create a testing db, enable PostGIS within it, and run the two torture tests.  The geo_torturetest is rather punative: 
```
cd /opt/installs/postgis-2.1.3/doc
xsltproc -o geo_torturetest.sql xsl/postgis_gardentest.sql.xsl postgis.xml
xsltproc -o rast_torturetest.sql xsl/raster_gardentest.sql.xsl postgis.xml
psql -U postgres -d postgres -c "DROP DATABASE testpostgis;"
psql -U postgres -d postgres -c "CREATE DATABASE testpostgis;" 
psql -U postgres -d testpostgis -c "CREATE EXTENSION postgis;" 
psql -U postgres -d testpostgis -f ../topology/topology.sql
psql -U postgres -d testpostgis -f ../postgis/sfcgal.sql
psql -U postgres -d testpostgis -f geo_torturetest.sql > geo_torturetest_results.txt 
psql -U postgres -d testpostgis -f rast_torturetest.sql > rast_torturetest_results.txt
```

